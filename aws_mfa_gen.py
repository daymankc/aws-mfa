""" Google Authenticator for GitLab """

import pyotp
import qrcode


# Gitlab 2FA Secret
secret = [YOUR_SECRET_FROM_AWS_MFA]
totp = pyotp.TOTP(secret)
print(totp.now())

# username = [YOUR_USERNAME_FROM_AWS]
# auth_str = t.provisioning_uri(
#     name = username,
#     issuer_name = "AWS-[COMPANY_NAME]",
# )
# print(auth_str)

# img = qrcode.make(auth_str)
# img.save("auth.png")